//
//  StgBrowser.h
//  GSStingray
//
//  Created by Dan on 12.11.2021.
//

#import "StgMain.h"
#import "StgSchema.h"

@class StgBrowser;

@protocol StgBrowserDelegate <NSObject>

@optional
- (void)stgBrowser:(StgBrowser *)sender endpointFound:(StgEndpoint *)endpoint;
- (void)stgBrowser:(StgBrowser *)sender endpointRemoved:(NSString *)name;

@end

@interface StgBrowserDelegates : NseArray <StgBrowserDelegate>

@end

@interface StgBrowser : NseNetServiceBrowser <NSNetServiceBrowserDelegate, NSNetServiceDelegate>

@property NSString *type;
@property StgBrowserDelegates *delegates;
@property NSMutableSet<NSNetService *> *services;

+ (instancetype)browserWithType:(NSString *)type;

- (void)start;

@end
