//
//  StgSchema.h
//  GSStingray
//
//  Created by Dan on 12.11.2021.
//

#import "StgMain.h"

#pragma mark - Channel

@interface StgChannel : NseObject

@property NSString *channelListId;
@property NSInteger channelNumber;
@property NSString *channelListName;
@property NSString *channelName;
@property BOOL visible;

+ (instancetype)from:(STGChannel *)sdkChannel;
- (STGChannel *)to;

@end

@interface NSArray (StgChannel)

+ (instancetype)stgChannelFrom:(GList *)sdkChannels;

@end

#pragma mark - Endpoint

@interface StgEndpoint : NseObject

@property NSString *name;
@property NSString *ip;
@property NSInteger port;
@property NSString *model;
@property NSString *version;

+ (instancetype)from:(STGEndpoint *)sdkEndpoint;
- (STGEndpoint *)to;

@end
