//
//  StgClient.m
//  GSStingray
//
//  Created by Dan on 12.11.2021.
//

#import "StgClient.h"

@implementation StgClient

- (void)dealloc {
    g_object_unref(self.object);
}

+ (instancetype)clientWithBase:(NSString *)base userAgent:(NSString *)userAgent {
    StgClient *ret = self.new;
    g_autoptr(SoupURI) sdkBase = soup_uri_new(base.UTF8String);
    ret.object = stg_soe_session_new(sdkBase, (gchar *)userAgent.UTF8String);
    ret.base = base;
    ret.userAgent = userAgent;
    return ret;
}

+ (instancetype)clientWithEndpoint:(StgEndpoint *)endpoint userAgent:(NSString *)userAgent {
    NSURLComponents *base = NSURLComponents.new;
    base.scheme = @"http";
    base.host = endpoint.ip;
    base.port = @(endpoint.port);
    StgClient *ret = [self clientWithBase:base.string userAgent:userAgent];
    return ret;
}

- (void)abort {
    soup_session_abort(SOUP_SESSION(self.object));
}

#pragma mark - PowerSet

- (BOOL)powerSetSync:(BOOL)on error:(NSError **)error {
    BOOL ret = NO;
    g_autoptr(GError) sdkError = NULL;
    
    if (stg_soe_session_power_set_sync(self.object, on, &sdkError)) {
        ret = YES;
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

- (void)powerSetAsync:(BOOL)on completion:(StgClientPowerSetAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        (void)[self powerSetSync:on error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, error);
        }];
    });
}

#pragma mark - ChannelsGet

- (NSArray<StgChannel *> *)channelsGetSync:(NSError **)error {
    NSArray<StgChannel *> *ret = nil;
    g_autolist(STGChannel) sdkRet = NULL;
    g_autoptr(GError) sdkError = NULL;
    
    if (stg_soe_session_channels_get_sync(self.object, &sdkRet, &sdkError)) {
        ret = [NSArray stgChannelFrom:sdkRet];
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

- (void)channelsGetAsync:(StgClientChannelsGetAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        NSArray<StgChannel *> *channels = [self channelsGetSync:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, channels, error);
        }];
    });
}

#pragma mark - ChannelSet

- (BOOL)channelSetSync:(StgChannel *)channel error:(NSError **)error {
    BOOL ret = NO;
    g_autoptr(STGChannel) sdkChannel = channel.to;
    g_autoptr(GError) sdkError = NULL;
    
    if (stg_soe_session_channel_set_sync(self.object, sdkChannel, &sdkError)) {
        ret = YES;
    } else {
        GE_SET_VALUE(error, [NSError gErrorFrom:sdkError]);
    }
    
    return ret;
}

- (void)channelSetAsync:(StgChannel *)channel completion:(StgClientChannelSetAsyncCompletion)completion {
    dispatch_queue_global_t queue = dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0);
    
    dispatch_async(queue, ^{
        NSError *error = nil;
        (void)[self channelSetSync:channel error:&error];
        
        [NSRunLoop.mainRunLoop performBlock:^{
            GE_CALL_FUNCTION(completion, error);
        }];
    });
}

@end
