//
//  StgBrowser.m
//  GSStingray
//
//  Created by Dan on 12.11.2021.
//

#import "StgBrowser.h"

@implementation StgBrowserDelegates

@end

@implementation StgBrowser

+ (instancetype)browserWithType:(NSString *)type {
    StgBrowser *ret = self.new;
    ret.type = type;
    ret.delegates = [StgBrowserDelegates.alloc initWithStorage:NSPointerArray.weakObjectsPointerArray];
    ret.delegate = ret;
    ret.services = NSMutableSet.set;
    return ret;
}

- (void)start {
    [self stop];
    [self searchForServicesOfType:self.type inDomain:@"local"];
}

- (void)netServiceBrowser:(NSNetServiceBrowser *)browser didFindService:(NSNetService *)service moreComing:(BOOL)moreComing {
    [self.services addObject:service];
    service.delegate = self;
    [service resolveWithTimeout:10.0];
}

- (void)netServiceBrowser:(NSNetServiceBrowser *)browser didRemoveService:(NSNetService *)service moreComing:(BOOL)moreComing {
    [self.delegates stgBrowser:self endpointRemoved:service.name];
}

- (void)netServiceDidResolveAddress:(NSNetService *)sender {
    [self.services removeObject:sender];
    
    for (NSData *address in sender.addresses) {
        struct sockaddr *addr = (struct sockaddr *)address.bytes;
        gchar ip[INET_ADDRSTRLEN];
        gchar ip6[INET6_ADDRSTRLEN];
        
        NSDictionary<NSString *, NSData *> *txt = [NSNetService dictionaryFromTXTRecordData:sender.TXTRecordData];
        NSData *model = txt[@"model"];
        NSData *version = txt[@"txtvers"];
        
        StgEndpoint *endpoint = StgEndpoint.new;
        endpoint.name = sender.name;
        endpoint.ip = (addr->sa_family == AF_INET) ? @(inet_ntop(AF_INET, &((struct sockaddr_in *)addr)->sin_addr, ip, INET_ADDRSTRLEN)) : @(inet_ntop(AF_INET6, &((struct sockaddr_in6 *)addr)->sin6_addr, ip6, INET6_ADDRSTRLEN));
        endpoint.port = sender.port;
        endpoint.model = [NSString.alloc initWithData:model encoding:NSUTF8StringEncoding];
        endpoint.version = [NSString.alloc initWithData:version encoding:NSUTF8StringEncoding];

        [self.delegates stgBrowser:self endpointFound:endpoint];
    }
}

- (void)netService:(NSNetService *)sender didNotResolve:(NSDictionary<NSString *, NSNumber *> *)errorDict {
    [self.services removeObject:sender];
}

@end
