//
//  StgMain.h
//  GSStingray
//
//  Created by Dan Kalinin on 8/23/20.
//

#import <gs-stingray/gs-stingray.h>
#import <GLibExt/GLibExt.h>
#import <SoupExt/SoupExt.h>
#import <GnuTLSExt/GnuTLSExt.h>
#import <GLibNetworkingExt/GLibNetworkingExt.h>
#import <JsonExt/JsonExt.h>
#import <AvahiExt/AvahiExt.h>
