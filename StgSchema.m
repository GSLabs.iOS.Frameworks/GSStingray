//
//  StgSchema.m
//  GSStingray
//
//  Created by Dan on 12.11.2021.
//

#import "StgSchema.h"

#pragma mark - Channel

@implementation StgChannel

+ (instancetype)from:(STGChannel *)sdkChannel {
    StgChannel *ret = self.new;
    ret.channelListId = NSE_BOX(sdkChannel->channel_list_id);
    ret.channelNumber = sdkChannel->channel_number;
    ret.channelListName = NSE_BOX(sdkChannel->channel_list_name);
    ret.channelName = NSE_BOX(sdkChannel->channel_name);
    ret.visible = sdkChannel->visible;
    return ret;
}

- (STGChannel *)to {
    STGChannel *ret = g_new0(STGChannel, 1);
    stg_channel_set_channel_list_id(ret, (gchar *)self.channelListId.UTF8String);
    ret->channel_number = (gint)self.channelNumber;
    stg_channel_set_channel_list_name(ret, (gchar *)self.channelListName.UTF8String);
    stg_channel_set_channel_name(ret, (gchar *)self.channelName.UTF8String);
    ret->visible = self.visible;
    return ret;
}

@end

@implementation NSArray (StgChannel)

+ (instancetype)stgChannelFrom:(GList *)sdkChannels {
    NSMutableArray<StgChannel *> *ret = NSMutableArray.array;
    
    for (GList *sdkChannel = sdkChannels; sdkChannel != NULL; sdkChannel = sdkChannel->next) {
        StgChannel *channel = [StgChannel from:sdkChannel->data];
        [ret addObject:channel];
    }
    
    return ret;
}

@end

#pragma mark - Endpoint

@implementation StgEndpoint

+ (instancetype)from:(STGEndpoint *)sdkEndpoint {
    StgEndpoint *ret = self.new;
    ret.name = NSE_BOX(sdkEndpoint->name);
    ret.ip = NSE_BOX(sdkEndpoint->ip);
    ret.port = sdkEndpoint->port;
    ret.model = NSE_BOX(sdkEndpoint->model);
    ret.version = NSE_BOX(sdkEndpoint->version);
    return ret;
}

- (STGEndpoint *)to {
    STGEndpoint *ret = g_new0(STGEndpoint, 1);
    stg_endpoint_set_name(ret, (gchar *)self.name.UTF8String);
    stg_endpoint_set_ip(ret, (gchar *)self.ip.UTF8String);
    ret->port = (gint)self.port;
    stg_endpoint_set_model(ret, (gchar *)self.model.UTF8String);
    stg_endpoint_set_version(ret, (gchar *)self.version.UTF8String);
    return ret;
}

@end
